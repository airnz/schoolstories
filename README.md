# SchoolStories - GovHack NZ 2015

**Client Side**

JavaScript Libraries

jQuery
https://jquery.com/

Leaflet JS
http://leafletjs.com/

Charting
https://developers.google.com/chart/

HTML 5 Shiv (Improves IE compatibility - JS)
https://github.com/aFarkas/html5shiv

Respond (Improves IE compatibility - CSS)
https://github.com/scottjehl/Respond


CSS - Responsive (Browser / Tablet / Mobiles)

Twitter Bootstrap
http://getbootstrap.com/


**Server Side Technology**

Java 8
http://www.oracle.com/technetwork/java/index.html

Spring (Spring Boot / Spring MVC / Spring Data)
https://spring.io/

Open CSV
http://opencsv.sourceforge.net/

Elastic Search
https://www.elastic.co/

Tomcat
http://tomcat.apache.org/


**Maps**

Open Street Map
https://www.openstreetmap.org/

Map Quest
http://open.mapquest.com/


**Hosting**

Amazon Web Services (EC2 / Amazon Linux AMI)
http://aws.amazon.com/


**Data Source**

NZQA

Education Counts Statistics

Statistics NZ
