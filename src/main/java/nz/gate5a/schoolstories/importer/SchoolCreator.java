package nz.gate5a.schoolstories.importer;

import java.math.BigDecimal;

import nz.gate5a.schoolstories.model.School;

import org.springframework.util.StringUtils;


/**
 * Imports {@link School}s from a CSV
 *
 * @author rjsang
 */
public class SchoolCreator implements DocumentCreator<School> {

    @Override
    public School create(String[] fields) {
        try {
            School school = new School();
            school.setId(makeLong(fields[0]));
            school.setName(fields[1]);
            school.setTelephone(fields[2]);
            school.setFax(fields[3]);
            school.setEmail(fields[4]);
            school.setPrincipal(fields[5]);
            school.setWebsite(fields[6]);
            school.setStreet(fields[7]);
            school.setSuburb(fields[8]);
            school.setCity(fields[9]);
            school.setPostalAddress1(fields[10]);
            school.setPostalAddress2(fields[11]);
            school.setPostalAddress3(fields[12]);
            school.setPostcode(fields[13]);
            school.setUrbanArea(fields[14]);
            school.setSchoolType(fields[15]);
            school.setDefinition(fields[16]);
            school.setAuthority(fields[17]);
            school.setGenderOfStudents(fields[18]);
            school.setTerritorialAuthorityWithAucklandLocalBoard(fields[19]);
            school.setRegionalCouncil(fields[20]);
            school.setMinistryLocalOffice(fields[21]);
            school.setEducationRegion(fields[22]);
            school.setGeneralElectorate(fields[23]);
            school.setMaoriElectorate(fields[24]);
            school.setCensusAreaUnit(fields[25]);
            school.setWard(fields[26]);
            school.setLongitude(bigDecimal(fields[27]));
            school.setLatitude(bigDecimal(fields[28]));
            school.setDecile(integer(fields[29]));
            school.setTotalSchoolRoll(integer(fields[30]));
            school.setEuropeanRoll(integer(fields[31]));
            school.setMaoriRoll(integer(fields[32]));
            school.setPasifikaRoll(integer(fields[33]));
            school.setAsian(integer(fields[34]));
            school.setMelaaRoll(integer(fields[35]));
            school.setOtherRoll(integer(fields[36]));
            school.setInternationalStudents(integer(fields[37]));
            return school;
        } catch (RuntimeException e) {
            throw new RuntimeException("Failed to parse school " + fields[0] + " " + fields[1], e);
        }
    }

    private BigDecimal bigDecimal(String value) {
        try {
            if (StringUtils.isEmpty(value)) {
                return null;
            }
            return new BigDecimal(value);
        } catch (NumberFormatException e) {
            throw new NumberFormatException(value);
        }
    }

    private Integer integer(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return Integer.valueOf(value);
    }
    
    private Long makeLong(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return Long.valueOf(value);
    }

	@Override
	public int numHeadingLinesInDocument() {
		return 4;
	}
}
