package nz.gate5a.schoolstories.importer;

import java.math.BigDecimal;

import nz.gate5a.schoolstories.model.School;
import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;

import org.springframework.util.StringUtils;

import java.util.UUID;

/**
 * Imports {@link School}s from a CSV
 *
 * @author rjsang
 */
public class NumeracyLiteracyCreator implements DocumentCreator<SchoolNumeracyLiteracy> {
    
    @Override
    public SchoolNumeracyLiteracy create(String[] fields) {
        try {
        	SchoolNumeracyLiteracy school = new SchoolNumeracyLiteracy();
        	// "Academic Year","Decile","Region","School","Year Level","Achievement","Current Year Achievement Rate","Cumulative Achievement Rate","Cohort Warning"
                school.setId(UUID.randomUUID().toString());
        	school.setAcademicYear(integer(fields[0]));
        	school.setDecile(integer(trimmed(fields[1])));
        	school.setRegion(fields[2]);
        	school.setSchool(fields[3]);
        	school.setYearLevel(integer(fields[4]));
        	school.setAchievement(fields[5]);
        	school.setCurrentYearAchievementRate(bigDecimal(fields[6]));
          	school.setCumulativeAchievementRate(bigDecimal(fields[7]));
          	school.setCohortWarning(fields[8]);
        	
            return school;
        } catch (RuntimeException e) {
            throw new RuntimeException("Failed to parse school " + fields[0] + " " + fields[1], e);
        }
    }

    private BigDecimal bigDecimal(String value) {
        try {
            if (StringUtils.isEmpty(value)) {
                return null;
            }
            return new BigDecimal(value);
        } catch (NumberFormatException e) {
            throw new NumberFormatException(value);
        }
    }

    private String trimmed(String value) {
        if (StringUtils.isEmpty(value)) {
            return "";
        }
        return value.trim();
    }
    
    private Integer integer(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return Integer.valueOf(value);
    }
    
    private Long makeLong(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return Long.valueOf(value);
    }

	@Override
	public int numHeadingLinesInDocument() {
		// TODO Auto-generated method stub
		return 1;
	}
}
