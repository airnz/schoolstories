/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.gate5a.schoolstories.importer;

import au.com.bytecode.opencsv.CSVReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import nz.gate5a.schoolstories.model.School;
import nz.gate5a.schoolstories.repo.SchoolRepository;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 *
 * @author rjsang
 */
public class CsvImporter<T> {
    
    private final InputStream source;

    private final ElasticsearchRepository<T, ?> repo;
    
    private final DocumentCreator<T> documentCreator;

    public CsvImporter(InputStream source, ElasticsearchRepository<T, ?> repo, DocumentCreator<T> documentCreator) {
        this.source = source;
        this.repo = repo;
        this.documentCreator = documentCreator;
    }

    public void go() throws IOException {
        try {
            CSVReader reader = new CSVReader(new InputStreamReader(source, "UTF-8"));
            String[] fields;

            // Skip headers and go to first line of schools
            for (int i = 0; i < documentCreator.numHeadingLinesInDocument(); i++) {
                reader.readNext();
            }

            while ((fields = reader.readNext()) != null) {
                T t = documentCreator.create(fields);
                repo.save(t);
            }
        } catch (UnsupportedEncodingException ex) {
            // This never happens!
            // Worst checked exception ever!
            throw new RuntimeException(ex);
        }
    }
    
}
