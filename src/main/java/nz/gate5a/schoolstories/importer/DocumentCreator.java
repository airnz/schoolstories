/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.gate5a.schoolstories.importer;

/**
 * Creates an object from a line of CSV
 * 
 * @author rjsang
 */
public interface DocumentCreator<T> {
    
    public T create(String[] lines);
    
    public int numHeadingLinesInDocument();
    
}
