/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.gate5a.schoolstories.importer;

import java.math.BigDecimal;
import java.util.UUID;
import nz.gate5a.schoolstories.model.SchoolNceaQualifications;
import org.springframework.util.StringUtils;

/**
 *
 * @author rjsang
 */
public class NceaQualificationCreator implements DocumentCreator<SchoolNceaQualifications> {

    @Override
    public SchoolNceaQualifications create(String[] fields) {
        try {
            SchoolNceaQualifications school = new SchoolNceaQualifications();
            
            school.setId(UUID.randomUUID().toString());
            school.setAcademicYear(integer(fields[0]));
            school.setDecile(integer(trimmed(fields[1])));
            school.setRegion(fields[2]);
            school.setSchool(fields[3]);
            school.setYearLevel(integer(fields[4]));

            school.setQualification(fields[5]);
            school.setCurrentYearAchievementRateParticipation(bigDecimal(fields[6]));
            school.setCumulativeAchievementRateParticipation(bigDecimal(fields[7]));
            school.setCurrentYearAchievementRateRoll(bigDecimal(fields[8]));
            school.setCumulativeAchievementRateRoll(bigDecimal(fields[9]));
            school.setCohortWarning(fields[10]);

            return school;
        } catch (RuntimeException e) {
            throw new RuntimeException("Failed to parse school " + fields[0] + " " + fields[1], e);
        }
    }

    private BigDecimal bigDecimal(String value) {
        try {
            if (StringUtils.isEmpty(value)) {
                return null;
            }
            return new BigDecimal(value);
        } catch (NumberFormatException e) {
            throw new NumberFormatException(value);
        }
    }

    private String trimmed(String value) {
        if (StringUtils.isEmpty(value)) {
            return "";
        }
        return value.trim();
    }

    private Integer integer(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return Integer.valueOf(value);
    }

    private Long makeLong(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return Long.valueOf(value);
    }

	@Override
	public int numHeadingLinesInDocument() {
		// TODO Auto-generated method stub
		return 1;
	}

}
