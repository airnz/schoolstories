package nz.gate5a.schoolstories.model;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * A School, as parsed from
 * http://www.educationcounts.govt.nz/__data/assets/file/0003/62571/Directory-School-current.csv
 *
 * @author rjsang
 */
@Document(indexName = "schools", type = "school", shards = 1, replicas = 0, refreshInterval = "-1")
public class School {

    @Id
    private Long id;
    
    private String name;
    
    private String telephone;
    
    private String fax;
    
    private String email;
    
    private String principal;
    
    private String website;
    
    private String street;
    
    private String suburb;
    
    private String city;
    
    private String postalAddress1;
    
    private String postalAddress2;
    
    private String postalAddress3;
    
    private String postcode;
    
    private String urbanArea;
    
    private String schoolType;
    
    private String definition;
    
    private String authority;
    
    private String genderOfStudents;
    
    private String territorialAuthorityWithAucklandLocalBoard;
    
    private String regionalCouncil;
    
    private String ministryLocalOffice;
    
    private String educationRegion;
    
    private String generalElectorate;
    
    private String maoriElectorate;
    
    private String censusAreaUnit;
    
    private String ward;
    
    private BigDecimal longitude;
    
    private BigDecimal latitude;
    
    private Integer decile;
    
    private Integer totalSchoolRoll;
    
    private Integer europeanRoll;
    
    private Integer maoriRoll;
    
    private Integer pasifikaRoll;
    
    private Integer asian;
    
    private Integer melaaRoll;
    
    private Integer otherRoll;
    
    private Integer internationalStudents;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalAddress1() {
        return postalAddress1;
    }

    public void setPostalAddress1(String postalAddress1) {
        this.postalAddress1 = postalAddress1;
    }

    public String getPostalAddress2() {
        return postalAddress2;
    }

    public void setPostalAddress2(String postalAddress2) {
        this.postalAddress2 = postalAddress2;
    }

    public String getPostalAddress3() {
        return postalAddress3;
    }

    public void setPostalAddress3(String postalAddress3) {
        this.postalAddress3 = postalAddress3;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getUrbanArea() {
        return urbanArea;
    }

    public void setUrbanArea(String urbanArea) {
        this.urbanArea = urbanArea;
    }

    public String getSchoolType() {
        return schoolType;
    }

    public void setSchoolType(String schoolType) {
        this.schoolType = schoolType;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getGenderOfStudents() {
        return genderOfStudents;
    }

    public void setGenderOfStudents(String genderOfStudents) {
        this.genderOfStudents = genderOfStudents;
    }

    public String getTerritorialAuthorityWithAucklandLocalBoard() {
        return territorialAuthorityWithAucklandLocalBoard;
    }

    public void setTerritorialAuthorityWithAucklandLocalBoard(String territorialAuthorityWithAucklandLocalBoard) {
        this.territorialAuthorityWithAucklandLocalBoard = territorialAuthorityWithAucklandLocalBoard;
    }

    public String getRegionalCouncil() {
        return regionalCouncil;
    }

    public void setRegionalCouncil(String regionalCouncil) {
        this.regionalCouncil = regionalCouncil;
    }

    public String getMinistryLocalOffice() {
        return ministryLocalOffice;
    }

    public void setMinistryLocalOffice(String ministryLocalOffice) {
        this.ministryLocalOffice = ministryLocalOffice;
    }

    public String getEducationRegion() {
        return educationRegion;
    }

    public void setEducationRegion(String educationRegion) {
        this.educationRegion = educationRegion;
    }

    public String getGeneralElectorate() {
        return generalElectorate;
    }

    public void setGeneralElectorate(String generalElectorate) {
        this.generalElectorate = generalElectorate;
    }

    public String getMaoriElectorate() {
        return maoriElectorate;
    }

    public void setMaoriElectorate(String maoriElectorate) {
        this.maoriElectorate = maoriElectorate;
    }

    public String getCensusAreaUnit() {
        return censusAreaUnit;
    }

    public void setCensusAreaUnit(String censusAreaUnit) {
        this.censusAreaUnit = censusAreaUnit;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public Integer getDecile() {
        return decile;
    }

    public void setDecile(Integer decile) {
        this.decile = decile;
    }

    public Integer getTotalSchoolRoll() {
        return totalSchoolRoll;
    }

    public void setTotalSchoolRoll(Integer totalSchoolRoll) {
        this.totalSchoolRoll = totalSchoolRoll;
    }

    public Integer getEuropeanRoll() {
        return europeanRoll;
    }

    public void setEuropeanRoll(Integer europeanRoll) {
        this.europeanRoll = europeanRoll;
    }

    public Integer getMaoriRoll() {
        return maoriRoll;
    }

    public void setMaoriRoll(Integer maoriRoll) {
        this.maoriRoll = maoriRoll;
    }

    public Integer getPasifikaRoll() {
        return pasifikaRoll;
    }

    public void setPasifikaRoll(Integer pasifikaRoll) {
        this.pasifikaRoll = pasifikaRoll;
    }

    public Integer getAsian() {
        return asian;
    }

    public void setAsian(Integer asian) {
        this.asian = asian;
    }

    public Integer getMelaaRoll() {
        return melaaRoll;
    }

    public void setMelaaRoll(Integer melaaRoll) {
        this.melaaRoll = melaaRoll;
    }

    public Integer getOtherRoll() {
        return otherRoll;
    }

    public void setOtherRoll(Integer otherRoll) {
        this.otherRoll = otherRoll;
    }

    public Integer getInternationalStudents() {
        return internationalStudents;
    }

    public void setInternationalStudents(Integer internationalStudents) {
        this.internationalStudents = internationalStudents;
    }

}
