package nz.gate5a.schoolstories.model;

import java.math.BigDecimal;

/**
 * Summarises a {@link School}
 * 
 * @author rjsang
 */
public class SchoolSummary {
    
    private Long id;
    
    private String name;
    
    private BigDecimal latitude;
    
    private BigDecimal longitude;

    public SchoolSummary(Long id, String name, BigDecimal latitude, BigDecimal longitude) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
    
}
