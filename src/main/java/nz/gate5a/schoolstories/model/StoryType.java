package nz.gate5a.schoolstories.model;

/**
 *
 * @author rjsang
 */
public enum StoryType {
    
    /** Specific to a school */
    SCHOOL, 
    
    /** Specific to a region */
    REGIONAL, 
    
    /** Applies to all schools */
    NATIONAL;
    
}
