package nz.gate5a.schoolstories.model;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author rjsang
 */
public class Chart {
    
    private List<DataPoint> data;
    
    private String label;

    public Chart() {
        data = new LinkedList<>();
    }

    public List<DataPoint> getData() {
        return data;
    }

    public void setData(List<DataPoint> data) {
        this.data = data;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    public static class DataPoint {
        private int year;
        private int score;

        public DataPoint(int year, int score) {
            this.year = year;
            this.score = score;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }
        
    }
    
            
            
}
