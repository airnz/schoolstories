package nz.gate5a.schoolstories.model;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * A user-submitted story about a school
 * 
 * @author rjsang
 */
@Document(indexName = "schools", type = "story", shards = 1, replicas = 0, refreshInterval = "-1")
public class Story {
    
    @Id
    private String id;
    
    private String author;
    
    private String content;
    
    private int year;
    
    private StoryType storyType;
    
    /** For SCHOOL story types */
    private String schoolName;
    
    /** For REGIONAL story types */
    private String region;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public StoryType getStoryType() {
        return storyType;
    }

    public void setStoryType(StoryType storyType) {
        this.storyType = storyType;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
    
}
