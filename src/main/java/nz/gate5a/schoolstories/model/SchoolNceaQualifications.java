package nz.gate5a.schoolstories.model;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 *
 * @author rjsang
 */
@Document(indexName = "schools", type = "schoolncea", shards = 1, replicas = 0, refreshInterval = "-1")
public class SchoolNceaQualifications {
    
    @Id
    private String id;

    private Integer academicYear;

    private Integer decile;

    private String region;

    private String school;

    private Integer yearLevel;

    private String qualification;

    private BigDecimal currentYearAchievementRateParticipation;

    private BigDecimal cumulativeAchievementRateParticipation;
    
    private BigDecimal currentYearAchievementRateRoll;

    private BigDecimal cumulativeAchievementRateRoll;

    private String CohortWarning;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(Integer academicYear) {
        this.academicYear = academicYear;
    }

    public Integer getDecile() {
        return decile;
    }

    public void setDecile(Integer decile) {
        this.decile = decile;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Integer getYearLevel() {
        return yearLevel;
    }

    public void setYearLevel(Integer yearLevel) {
        this.yearLevel = yearLevel;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public BigDecimal getCurrentYearAchievementRateParticipation() {
        return currentYearAchievementRateParticipation;
    }

    public void setCurrentYearAchievementRateParticipation(BigDecimal currentYearAchievementRateParticipation) {
        this.currentYearAchievementRateParticipation = currentYearAchievementRateParticipation;
    }

    public BigDecimal getCumulativeAchievementRateParticipation() {
        return cumulativeAchievementRateParticipation;
    }

    public void setCumulativeAchievementRateParticipation(BigDecimal cumulativeAchievementRateParticipation) {
        this.cumulativeAchievementRateParticipation = cumulativeAchievementRateParticipation;
    }

    public BigDecimal getCurrentYearAchievementRateRoll() {
        return currentYearAchievementRateRoll;
    }

    public void setCurrentYearAchievementRateRoll(BigDecimal currentYearAchievementRateRoll) {
        this.currentYearAchievementRateRoll = currentYearAchievementRateRoll;
    }

    public BigDecimal getCumulativeAchievementRateRoll() {
        return cumulativeAchievementRateRoll;
    }

    public void setCumulativeAchievementRateRoll(BigDecimal cumulativeAchievementRateRoll) {
        this.cumulativeAchievementRateRoll = cumulativeAchievementRateRoll;
    }

    public String getCohortWarning() {
        return CohortWarning;
    }

    public void setCohortWarning(String CohortWarning) {
        this.CohortWarning = CohortWarning;
    }


    
}
