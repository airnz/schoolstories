package nz.gate5a.schoolstories.model;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * A SchoolNumeracyGender, as parsed from
 * http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2014/Literacy-and-Numeracy-Statistics-School-Gender-2014-17042015.csv
 *
 * @author darrenr
 */
@Document(indexName = "schools", type = "schoolNumeracyGender", shards = 1, replicas = 0, refreshInterval = "-1")
public class SchoolNumeracyLiteracyGender {

    @Id
    private Long id;
    
    private Integer academicYear;
    
    private Integer decile;
    
    private String region;
    
    private String school;
    
    private Integer yearLevel;
    
    private String gender;
 
	private String Achievement;
    
    private BigDecimal currentYearAchievementRate;
    
    private BigDecimal cumulativeAchievementRate;
    
    private String CohortWarning;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(Integer academicYear) {
		this.academicYear = academicYear;
	}

	public Integer getDecile() {
		return decile;
	}

	public void setDecile(Integer decile) {
		this.decile = decile;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public Integer getYearLevel() {
		return yearLevel;
	}

	public void setYearLevel(Integer yearLevel) {
		this.yearLevel = yearLevel;
	}

	public String getAchievement() {
		return Achievement;
	}

	public void setAchievement(String achievement) {
		Achievement = achievement;
	}

	public BigDecimal getCurrentYearAchievementRate() {
		return currentYearAchievementRate;
	}

	public void setCurrentYearAchievementRate(BigDecimal currentYearAchievementRate) {
		this.currentYearAchievementRate = currentYearAchievementRate;
	}

	public BigDecimal getCumulativeAchievementRate() {
		return cumulativeAchievementRate;
	}

	public void setCumulativeAchievementRate(BigDecimal cumulativeAchievementRate) {
		this.cumulativeAchievementRate = cumulativeAchievementRate;
	}

	public String getCohortWarning() {
		return CohortWarning;
	}

	public void setCohortWarning(String cohortWarning) {
		CohortWarning = cohortWarning;
	}
    
    public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
    
}
