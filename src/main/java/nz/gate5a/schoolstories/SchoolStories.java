package nz.gate5a.schoolstories;

import java.io.InputStream;
import java.net.URL;

import java.util.UUID;

import nz.gate5a.schoolstories.importer.CsvImporter;
import nz.gate5a.schoolstories.importer.SchoolCreator;
import nz.gate5a.schoolstories.importer.NceaQualificationCreator;
import nz.gate5a.schoolstories.model.Story;
import nz.gate5a.schoolstories.model.StoryType;
import nz.gate5a.schoolstories.importer.NumeracyLiteracyCreator;
import nz.gate5a.schoolstories.repo.NceaRepository;
import nz.gate5a.schoolstories.repo.SchoolNumeracyLiteracyRepository;
import nz.gate5a.schoolstories.repo.SchoolRepository;
import nz.gate5a.schoolstories.repo.StoryRepository;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SchoolStories {

    public static void main(String[] args) {
        SpringApplication.run(SchoolStories.class, args);
    }

    @Bean
    public InitializingBean importSchools(SchoolRepository schoolRepository, @Value("${reload.schools}") Boolean reload) {
        return () -> {

            if (reload) {
                System.out.println("Loading schools");
                schoolRepository.deleteAll();
                InputStream input = new URL("http://www.educationcounts.govt.nz/__data/assets/file/0003/62571/Directory-School-current.csv").openStream();
                CsvImporter importer = new CsvImporter(input, schoolRepository, new SchoolCreator());
                importer.go();
                System.out.println("Done Loading schools");
            }
        };
    }

    @Bean
    public InitializingBean importSchoolsNumeracyLiteracy(SchoolNumeracyLiteracyRepository repository, @Value("${reload.schoolnumeracyliteracy}") Boolean reload) {
        return () -> {
            if (reload) {
                String[] dataSetsByYear = {
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2014/Literacy-and-Numeracy-Statistics-School-2014-17042015.csv",
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2013/Literacy-and-Numeracy-Statistics-School-2013-01052014.csv",
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2012/Literacy-and-Numeracy-Statistics-School-2012-21032013.csv",
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2011/Literacy-and-Numeracy-Statistics-School-2011-21032013.csv",
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2010/Literacy-and-Numeracy-Statistics-School-2010-21032013.csv",
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2009/Literacy-and-Numeracy-Statistics-School-2009-21032013.csv",
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2008/Literacy-and-Numeracy-Statistics-School-2008-21032013.csv",
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2007/Literacy-and-Numeracy-Statistics-School-2007-21032013.csv",
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2006/Literacy-and-Numeracy-Statistics-School-2006-21032013.csv",
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2005/Literacy-and-Numeracy-Statistics-School-2005-21032013.csv",
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2004/Literacy-and-Numeracy-Statistics-School-2004-21032013.csv"
                };
               
                repository.deleteAll();
                for (String dataset : dataSetsByYear) {
                    System.out.println("Loading school numeracy literacy data for: " + dataset);

                    InputStream input = new URL(dataset).openStream();
                    CsvImporter importer = new CsvImporter(input, repository, new NumeracyLiteracyCreator());
                    importer.go();
                    System.out.println("Done Loading school numeracy literacy data for:");
                }
            }
        };
    }
    
    @Bean
    public InitializingBean importSchoolsNcea(NceaRepository repository, @Value("${reload.schoolncea}") Boolean reload) {
        return () -> {
            if (reload) {
                String[] nceaDataSetsByYear = {
                    "http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2014/Qualification-Statistics-School-2014-17042015.csv",
					"http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2013/Qualification-Statistics-School-2013-01052014.csv",
					"http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2012/Qualification-Statistics-School-2012-21032013.csv",
					"http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2011/Qualification-Statistics-School-2011-21032013.csv",
					"http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2010/Qualification-Statistics-School-2010-21032013.csv",					
					"http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2009/Qualification-Statistics-School-2009-21032013.csv",	
					"http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2008/Qualification-Statistics-School-2008-21032013.csv",	
					"http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2007/Qualification-Statistics-School-2007-21032013.csv",
					"http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2006/Qualification-Statistics-School-2006-21032013.csv",	
					"http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2005/Qualification-Statistics-School-2005-21032013.csv",		
					"http://www.nzqa.govt.nz/assets/Studying-in-NZ/Secondary-school-and-NCEA/stats-reports/2004/Qualification-Statistics-School-2004-21032013.csv"									
                    };                
                repository.deleteAll();
                for (String dataset : nceaDataSetsByYear) {
                    System.out.println("Loading school ncea data for: " + dataset);

                    InputStream input = new URL(dataset).openStream();
                    CsvImporter importer = new CsvImporter(input, repository, new NceaQualificationCreator());
                    importer.go();
                    System.out.println("Done Loading school ncea data.");
                }
            }
        };
    }

}
