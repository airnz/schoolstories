package nz.gate5a.schoolstories.services;

import java.util.List;

import javax.inject.Inject;
import javax.naming.ldap.PagedResultsControl;

import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;
import nz.gate5a.schoolstories.repo.SchoolNumeracyLiteracyRepository;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rjsang
 */
@RestController
@RequestMapping("results")
public class ResultsService {
    
    @Inject
    private SchoolNumeracyLiteracyRepository repository;
    
    @RequestMapping("/literacy/{schoolname}")
    public List<SchoolNumeracyLiteracy> getLiteracyBySchoolName(@PathVariable("schoolname") String name) {
        return repository.findBySchoolAndAchievementIgnoringCase(new PageRequest(0, 1000), name, "Literacy");
    }
    
    @RequestMapping("/numeracy/{schoolname}")
    public List<SchoolNumeracyLiteracy> getNumeracyBySchoolName(@PathVariable("schoolname") String name) {
        return repository.findBySchoolAndAchievementIgnoringCase(new PageRequest(0, 1000), name, "Numeracy");
    }
    
    @RequestMapping("/forschool/{schoolname}")
    public List<SchoolNumeracyLiteracy> getAllBySchoolName(@PathVariable("schoolname") String name) {
        return repository.findBySchool(new PageRequest(0, 1000), name);
    }
    
    @RequestMapping("foryear/{year}")
    public List<SchoolNumeracyLiteracy> getAllForYear(@PathVariable("year") Integer year) {
        return repository.findByAcademicYearOrderBySchoolDesc(new PageRequest(0, 1000), year);
    }

}
