package nz.gate5a.schoolstories.services;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.inject.Inject;

import nz.gate5a.schoolstories.model.School;
import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;
import nz.gate5a.schoolstories.repo.SchoolNumeracyLiteracyRepository;
import nz.gate5a.schoolstories.repo.SchoolRepository;

import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rjsang
 */
@RestController
@RequestMapping("schools")
public class SchoolService {
    
    @Inject
    private SchoolRepository schoolRepository;
    
    @Inject
    private SchoolNumeracyLiteracyRepository numeracyLiteracyRepo;
    
    @RequestMapping("/byid/{id}")
    public School getSchoolById(@PathVariable("id") Long id) {
        return schoolRepository.findOne(id);
    }
    
    @RequestMapping("/byname/{name}")
    public List<School> getSchoolByName(@PathVariable("name") String name) {
        // TODO - not break working code, your friends are spartans you see? LOL
        List<School> schools = schoolRepository.findByNameIgnoringCase(new PageRequest(0, 1000), name);
        return schools.stream().filter(
                (School school) -> !numeracyLiteracyRepo.findBySchool(new PageRequest(0, 1000), "\"" + school.getName() + "\"").isEmpty())
                .collect(Collectors.toList());
    }
}
