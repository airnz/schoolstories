package nz.gate5a.schoolstories.services;

import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import nz.gate5a.schoolstories.model.School;
import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;
import nz.gate5a.schoolstories.model.SchoolSummary;
import nz.gate5a.schoolstories.repo.SchoolNumeracyLiteracyRepository;
import nz.gate5a.schoolstories.repo.SchoolRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rjsang
 */
@RestController
@RequestMapping("locate")
public class LocationService {
    
    @Inject
    private SchoolRepository repo;
    
    @Inject
    private SchoolNumeracyLiteracyRepository numeracyLiteracyRepo;
    
    @RequestMapping("schools")
    public List<SchoolSummary> getSchools() {
        Iterable<School> schools = repo.findAll();
        List<SchoolSummary> summaries = new LinkedList<>();
        schools.forEach((school) -> {
            // TODO - not break working code, your friends are spartans you see? LOL
            List<SchoolNumeracyLiteracy> l = numeracyLiteracyRepo.findBySchool(new PageRequest(0, 1000), "\"" + school.getName() + "\"");
            if(!l.isEmpty()) {
            summaries.add(new SchoolSummary(school.getId(), school.getName(), school.getLatitude(), school.getLongitude()));
            }
        });
        return summaries;
    }
    
}
