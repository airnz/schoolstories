package nz.gate5a.schoolstories.services;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

import nz.gate5a.schoolstories.model.Chart;
import nz.gate5a.schoolstories.model.SchoolNceaQualifications;
import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;
import nz.gate5a.schoolstories.repo.NceaRepository;
import nz.gate5a.schoolstories.repo.SchoolNumeracyLiteracyRepository;

import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rjsang
 */
@RestController
@RequestMapping("charts")
public class ChartService {
    
    @Inject
    private SchoolNumeracyLiteracyRepository numeracyLiteracyRepository;

    @Inject
    private NceaRepository nceaRepository;
    
    @RequestMapping("{achievement}")
    public Chart getAchievementForSchoolAndYear(@PathVariable("achievement") String achievement, 
            @RequestParam("school") String school, @RequestParam("year") int year) {
    	
    	final Chart chart = new Chart();
    	chart.setLabel(achievement+" - Year " + year);  // e.g. "Literacy Year
    	
    	// bit of a hack - should really have 2 classes but .... figure out if we need to go fetch NCEA or not
    	if (achievement.equalsIgnoreCase("literacy") || achievement.equalsIgnoreCase("numeracy")) {
    	
	        List<SchoolNumeracyLiteracy> results = numeracyLiteracyRepository.findBySchoolAndAchievementIgnoringCase(new PageRequest(0, 1000), "\"" + school + "\"", achievement);
	        
	        
	        results.forEach((result) -> {
	            if(result.getYearLevel().equals(year)) {
	                int score = result.getCumulativeAchievementRate().multiply(BigDecimal.valueOf(100l)).intValue();
	                chart.getData().add(new Chart.DataPoint(result.getAcademicYear(), score));
	            }
	        });
    	} else {
    	    List<SchoolNceaQualifications> results = nceaRepository.findBySchoolAndQualificationIgnoringCase(new PageRequest(0, 1000), "\"" + school + "\"", "\"" + achievement + "\"");
   	        
   	        results.forEach((result) -> {
   	            if(result.getYearLevel().equals(year)) {
   	                int score = result.getCurrentYearAchievementRateParticipation().multiply(BigDecimal.valueOf(100l)).intValue();
   	                chart.getData().add(new Chart.DataPoint(result.getAcademicYear(), score));
   	            }
   	        });	
    	}
        return chart;
    }
}
