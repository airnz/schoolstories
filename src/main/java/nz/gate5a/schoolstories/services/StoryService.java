package nz.gate5a.schoolstories.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;
import nz.gate5a.schoolstories.model.Story;
import nz.gate5a.schoolstories.model.StoryType;
import nz.gate5a.schoolstories.repo.SchoolNumeracyLiteracyRepository;
import nz.gate5a.schoolstories.repo.StoryRepository;

import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rjsang
 */
@RestController
@RequestMapping("stories")
public class StoryService {
    
    @Inject
    private StoryRepository storyRepository;
    
    @Inject
    private SchoolNumeracyLiteracyRepository numeracyLiteracyRepository;
    
    
    @RequestMapping(value = "add", method = RequestMethod.POST, consumes = "application/json")
    public Story addStory(@RequestBody Story story) {
        storyRepository.save(story);
        return story;
    }
    
    @RequestMapping("byid/{id}")
    public Story getStory(@PathVariable("id") String id) {
        return storyRepository.findOne(id);
    }
    
    @RequestMapping("bySchoolName/{schoolName}")
    public List<Story> getStoriesForSchool(@PathVariable("schoolName") String schoolName) {
        List<SchoolNumeracyLiteracy> list = numeracyLiteracyRepository.findBySchool(new PageRequest(0, 1000), "\"" + schoolName + "\"");
        if(list.isEmpty()) {
            return Collections.emptyList();
        }
        String region = list.get(0).getRegion();
        
        List<Story> stories = new ArrayList<>();
        stories.addAll(storyRepository.findBySchoolName(new PageRequest(0, 1000), schoolName));
        stories.addAll(storyRepository.findByRegion(new PageRequest(0, 1000), region));
        stories.addAll(storyRepository.findByStoryType(new PageRequest(0, 1000), StoryType.NATIONAL));
        
        return stories;
    }
    
    @RequestMapping("byRegion/{region}")
    public List<Story> getStoriesForRegion(@PathVariable("region") String region) {
        List<Story> stories = new ArrayList<>();
        stories.addAll(storyRepository.findByRegion(new PageRequest(0, 1000), region));
        stories.addAll(storyRepository.findByStoryType(new PageRequest(0, 1000), StoryType.NATIONAL));
        
        return stories;
    }
    
    
    
}
