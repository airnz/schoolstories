package nz.gate5a.schoolstories.repo;

import java.util.List;

import javax.naming.ldap.PagedResultsControl;

import nz.gate5a.schoolstories.model.School;
import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;

import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Repository of {@link School}s
 *
 * @author rjsang
 */
public interface SchoolNumeracyLiteracyRepository extends ElasticsearchRepository<SchoolNumeracyLiteracy, String> {

    List<SchoolNumeracyLiteracy> findBySchoolAndAchievementIgnoringCase(Pageable page, String name, String achievement);

    List<SchoolNumeracyLiteracy> findBySchool(Pageable page, String school);
    
    List<SchoolNumeracyLiteracy> findByAcademicYearOrderBySchoolDesc(Pageable page, Integer year);

}
