package nz.gate5a.schoolstories.repo;

import java.util.List;

import nz.gate5a.schoolstories.model.Story;
import nz.gate5a.schoolstories.model.StoryType;

import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 *
 * @author rjsang
 */
public interface StoryRepository extends ElasticsearchRepository<Story, String> {
    
    public List<Story> findBySchoolName(Pageable page, String schoolName);
    
    public List<Story> findByRegion(Pageable page, String region);
    
    public List<Story> findByStoryType(Pageable page, StoryType storyType);
}
