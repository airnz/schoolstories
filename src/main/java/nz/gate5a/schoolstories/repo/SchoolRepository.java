package nz.gate5a.schoolstories.repo;

import java.util.List;

import nz.gate5a.schoolstories.model.School;

import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Repository of {@link School}s
 *
 * @author rjsang
 */
public interface SchoolRepository extends ElasticsearchRepository<School, Long> {
    
    public List<School> findByNameIgnoringCase(Pageable page, String name);
    
}
