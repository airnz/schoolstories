package nz.gate5a.schoolstories.repo;

import java.util.List;

import javax.naming.ldap.PagedResultsControl;

import nz.gate5a.schoolstories.model.School;
import nz.gate5a.schoolstories.model.SchoolNceaQualifications;
import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Repository of {@link School}s
 *
 * @author rjsang
 */
public interface NceaRepository extends ElasticsearchRepository<SchoolNceaQualifications, String> {

	List<SchoolNceaQualifications> findBySchoolAndQualificationIgnoringCase(Pageable page, String name, String achievement);

}
