google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(function() {
	var data = getLiteracy();
	drawSchoolChart(data);
});

$("#literacy").on('click', function(evt) {
	evt.preventDefault();
	evt.stopPropagation();
	var data = getLiteracy();
	drawSchoolChart(data);
});

$("#numeracy").on('click', function(evt) {
	evt.preventDefault();
	evt.stopPropagation();
	var data = getNumeracy();
	drawSchoolChart(data);
});

function drawChart(data) {
    var options = {
        hAxis: {
            title: 'Year',
            format: '#'
        },
        vAxis: {
            title: 'Cumulative achievement (% of participants)'
        },
        tooltip: {
            trigger: 'both',
            isHtml: true
        }
    };

    var charElement = document.getElementById('school_chart');
    var chart = new google.visualization.LineChart(charElement);
    chart.draw(data, options);
}

function getNumeracy(schoolName) {
	var data = new google.visualization.DataTable();
	data.addColumn('number', 'Year');
    data.addColumn('number', 'Numeracy');
    data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});

    data.addRows([
      [2004, 10, '<b>2004</b>: 10%'], 
      [2005, 70, '2005: 20%'], 
      [2006, 30, '2006: 30%'], 
      [2007, 50, '2007: 50%<br/>New principal hired'], 
      [2008, 100, '2008: 100%'],
      [2009, 90, '2009: 90%<br />New principal fired'], 
      [2010, 70, '2010: 70%'], 
      [2011, 40, '2011: 40%'], 
      [2012, 30, '2012: 30%'], 
      [2013, 40, '2013: 40%'],
      [2014, 40, '2014: 40%']
    ]);
    return data;
}

function getLiteracy(schoolName) {
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Year');
    data.addColumn('number', 'Literacy');
    data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});

    data.addRows([
        [2004, 10, '<b>2004</b>: 10%'],
        [2005, 20, '2005: 20%'],
        [2006, 30, '2006: 30%'],
        [2007, 50, '2007: 50%<br/>New principal hired'],
        [2008, 100, '2008: 100%'],
        [2009, 90, '2009: 90%<br />New principal died'],
        [2010, 70, '2010: 70%'],
        [2011, 40, '2011: 40%'],
        [2012, 30, '2012: 30%'],
        [2013, 40, '2013: 40%'],
        [2014, 40, '2014: 40%']
    ]);
    return data;
}


function getDecile(schoolName) {
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Year');
    data.addColumn('number', 'Decile');
    data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});

    data.addRows([
        [2004, 30, '<b>2004</b>: 10%'],
        [2005, 40, '2005: 20%'],
        [2006, 35, '2006: 30%'],
        [2007, 60, '2007: 50%<br/>New principal hired'],
        [2008, 85, '2008: 100%'],
        [2009, 90, '2009: 90%<br />New principal died'],
        [2010, 70, '2010: 70%'],
        [2011, 40, '2011: 40%'],
        [2012, 30, '2012: 30%'],
        [2013, 40, '2013: 40%'],
        [2014, 40, '2014: 40%']
    ]);
    return data;
}