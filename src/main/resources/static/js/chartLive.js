google.load('visualization', '1', {packages: ['corechart', 'line']});
var currentAchievement = null;


$("#year11Events").on('click', function(){
    updateChart('11');
});

$("#year12Events").on('click', function(){
    updateChart('12');
});

$("#year13Events").on('click', function(){
    updateChart('13');
});

function updateChart(schoolYear){
    var selectedSchool = $("#schoolName").text();
    plotAchievement(selectedSchool, currentAchievement, schoolYear);
}

function getAchievement(schoolName, achievement){
    plotAchievement(schoolName, achievement, '11');
}

function plotAchievement(schoolName, achievement, schoolYear) {

    currentAchievement = achievement;

    $.getJSON("charts/" + achievement + "?school=" + schoolName + "&year="+schoolYear, function (results) {

        var data = new google.visualization.DataTable();
        data.addColumn('number', 'Year');
        data.addColumn('number', results.label);
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});

        var dataPoints = convertChartDataPoints(results.data)
        
        for(i = 0; i < dataPoints.length; i++) {
            var row = dataPoints[i];
            if(!schoolStories['' + row[0]]) {
                schoolStories['' + row[0]] = [];
            }
            row[2] = '<b>' + row[0] + '</b><br />' + row[1] + '%<br /><a onClick="openModal(\'' + schoolName + '\', ' + row[0] + ')" href="#">Stories: ' + schoolStories['' + row[0]].length + '</a>';
        }
        
        data.addRows(dataPoints);
        drawChart(data);
    });
}

function drawChart(data) {

    var options = {
        hAxis: {
            title: 'Year',
            format: '#'
        },
        vAxis: {
            title: 'Cumulative achievement (%)',
            minValue: 0,
            maxValue: 100
        },
        tooltip: {
            trigger: 'both',
            isHtml: true
        }
    };

    var chart = new google.visualization.LineChart(document.getElementById('school_chart'));
    chart.draw(data, options);
}

function convertChartDataPoints(dataPoints) {
    data = [];
    for(i = 0; i < dataPoints.length; i++) {
        data[i] = [dataPoints[i].year, dataPoints[i].score];
    }
    return data;
}
