var markers = new Array();

var map = L.map('map', {
    center: ['-41.2', '174.76'],
    zoom: 5
});

L.tileLayer('http://{s}.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright" title="OpenStreetMap" target="_blank">OpenStreetMap</a> contributors | Tiles Courtesy of <a href="http://www.mapquest.com/" title="MapQuest" target="_blank">MapQuest</a> | #GovHack2015 #Australia #NewZealand',
    subdomains: ['otile1', 'otile2', 'otile3', 'otile4']
}).addTo(map);

var myIcon = L.icon({
    iconUrl: 'img/pin24.png',
    iconRetinaUrl: 'img/pin48.png',
    iconSize: [29, 24],
    iconAnchor: [9, 21],
    popupAnchor: [0, -14]
});


$.getJSON("locate/schools", function (schoolData) {

    $.each(schoolData, function (index, school) {
        addMarker(school);
    });

});

schoolStories = {};

function displaySchoolInfo(schoolId) {
    $.getJSON("schools/byid/" + schoolId, function (school) {

        $("#schoolDetails").attr('class', 'visible');

        $("#schoolId").html(school.id);

        $("#schoolName").html(school.name);

        var schoolAddress = school.street + ", ";
        schoolAddress += school.suburb + ", ";
        schoolAddress += school.city;
        $("#schoolAddress").html(schoolAddress);
        
        $.getJSON("stories/bySchoolName/" + school.name, function(stories) {
            for(i = 0; i < stories.length; i++) {
                
                if(!schoolStories['' + stories[i].year]) {
                    schoolStories['' + stories[i].year] = [];
                }
                yearStories = schoolStories['' + stories[i].year];
                yearStories[yearStories.length] = stories[i];
            }
            
        });
        
    });
}

function deleteAllMarkers() {

    for (i = 0; i < markers.length; i++) {
        var marker = markers[i];
        map.removeLayer(marker);
    }
}

function addMarker(school) {
    if (school.latitude != null && school.longitude != null) {
        // Adding a marker only for schools with coordinates
        var marker = L.marker([school.latitude, school.longitude], {icon: myIcon})
            .bindPopup("<a id='" + school.id + "' class='schoolOnMapSelected' href='#' onclick='displaySchoolInfo(" + school.id + "); return false;' title='Click for details'>" + school.name + "</a>")
            .addTo(map);
        // We keep it on an array to remove / add dynamically
        markers.push(marker);
    }
}

function zoomTo(lat, long) {
    map.setView(new L.LatLng(lat, long), 14);
}

function openPopup(lat, long) {
    for (i = 0; i < markers.length; i++) {
        var marker = markers[i];
        var curPos = marker.getLatLng();
        if (curPos.lat == lat && curPos.lng == long) {
            marker.openPopup();
        }
    }
}
