$("#literacyInfo").on('click', function (evt) {
    makeChartHappen("#literacyInfo", "Literacy", evt);
});

$("#numeracyInfo").on('click', function (evt) {
    makeChartHappen("#numeracyInfo", "Numeracy", evt);
});

$("#NCEAL1Info").on('click', function (evt) {
    makeChartHappen("#NCEAL1Info", "NCEA Level 1", evt);
});

$("#NCEAL2Info").on('click', function (evt) {
    makeChartHappen("#NCEAL2Info", "NCEA Level 2", evt);
});

$("#NCEAL3Info").on('click', function (evt) {
    makeChartHappen("#NCEAL3Info", "NCEA Level 3", evt);
});

$("#UEInfo").on('click', function (evt) {
    makeChartHappen("#UEInfo", "University Entrance", evt);
});

function makeChartHappen(button, achievement, evt) {
    evt.stopImmediatePropagation();
    evt.preventDefault();

    hideMap();
    makeActive(button);
    makeYear11Active();

    var schoolName = $("#schoolName").text();
    getAchievement(schoolName, achievement);
}

$("#mapInfo").on('click', function (evt) {
    evt.stopImmediatePropagation();
    evt.preventDefault();

    makeActive("#mapInfo");
    hideChart();
});

function openModal(schoolName, year) {
    $("#myModalLabel").html('What happened at ' + schoolName + ' in ' + year + '?');
    var yearStories = schoolStories['' + year];
    var content = '';
    for (i = 0; i < yearStories.length; i++) {
        content = content + '<p><strong>' + yearStories[i].content + '!</strong><br />'
            + 'by ' + yearStories[i].author + '</p><hr />';
    }
    $("#modalEvents").html(content);
    $("#event-year").val(year);
    $("#myModal").modal('show');
}

function addStory() {
    var schoolName = $("#schoolName").text();

    var story = {};
    story.content = $("#event").val();
    story.author = $("#event-adder").val();
    story.year = $("#event-year").val();
    story.storyType = "SCHOOL";
    story.schoolName = schoolName;

    $.ajax({
        url: 'stories/add',
        type: "POST",
        dataType: "json", // expected format for response
        contentType: "application/json", // send as JSON
        data: JSON.stringify(story),

        success: function (result) {
            var content = $("#modalEvents").html();
            content = content + '<p><strong>' + result.content + '!</strong><br />'
                + 'by ' + result.author + '</p><hr />';
            $("#modalEvents").html(content);
            $("#event").val("");
            $("#event-adder").val("");
        },

    });

    return false;
}


$("#searchButton").on('click', function () {
    doSearch();
});

$("#searchFor").on("keydown", function (event) {
    if (event.which == 13) {
        doSearch();
    }
});

function doSearch() {
    // get the value and search using ajax
    var searchingForSchoolName = $("#searchFor").val();
    $.getJSON("schools/byname/" + searchingForSchoolName, function (schools) {
        if (schools != null) {
            // delete all markers
            deleteAllMarkers();
            // add the new markers
            $.each(schools, function (index, school) {
                addMarker(school);
            });

            // zoom and opens the pop up for the first one found
            var firstSchoolFound = schools[0];
            if (firstSchoolFound != null) {
                hideChart();
                makeActive("#mapInfo");
                zoomTo(firstSchoolFound.latitude, firstSchoolFound.longitude);
                openPopup(firstSchoolFound.latitude, firstSchoolFound.longitude);
            } else {
                alert('No search results, try again!');
            }
        }
    });
}

function hideMap() {
    hideAndShow("#map", "#chart");
}

function hideChart() {
    hideAndShow("#chart", "#map");
}

function hideAndShow(idHide, idShow) {
    // Hide
    $(idHide).css('visibility', 'hidden');
    $(idHide).css('display', 'none');
    // Show
    $(idShow).css('visibility', 'inherit');
    $(idShow).css('display', 'inherit');
}

function makeActive(myId) {
    // All of them reset
    $(".list-group-item").each(function (index, item) {
        $("#" + item.id).attr('class', 'list-group-item');
    });
    // The special one active
    $(myId).addClass('active');
}

function makeYear11Active(){
    $("#year11Events").removeAttr("checked");
    $("#year12Events").removeAttr("checked");
    $("#year13Events").removeAttr("checked");
    $("#year11Events").prop("checked", true);
}
