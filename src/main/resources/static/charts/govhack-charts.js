var data = {
	labels: ["2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", 
	         "2012", "2013", "2014", "2015"],
    datasets: [
    {
        label: "Literacy",
        fillColor: "rgba(220,220,220,0.2)",
        strokeColor: "rgba(220,220,220,1)",
        pointColor: "rgba(220,220,220,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(220,220,220,1)",
        data: [10, 59, 80, 81, 56, 55, 40, 35, 36, 37, 99, 55]
    },
    {
        label: "Numeracy",
        fillColor: "rgba(151,187,205,0.2)",
        strokeColor: "rgba(151,187,205,1)",
        pointColor: "rgba(151,187,205,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(151,187,205,1)",
        data: [28, 48, 40, 19, 86, 27, 90, 75, 12, 75, 99, 34]
    }
	]
};

var options = {
    scaleLabel: "<%= Number(value) / 100 %>",

    multiTooltipTemplate: "<%= datasetLabel %>",
    
    scaleBeginAtZero: true,
    
    animation: false,
    
    bezierCurve : false,
    
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines : true,

    //String - Colour of the grid lines
    scaleGridLineColor : "rgba(0,0,0,.05)",

    //Number - Width of the grid lines
    scaleGridLineWidth : 1,

    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,

    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,

    //Boolean - If there is a stroke on each bar
    barShowStroke : true,

    //Number - Pixel width of the bar stroke
    barStrokeWidth : 2,

    //Number - Spacing between each of the X value sets
    barValueSpacing : 2,

    //Number - Spacing between data sets within X values
    barDatasetSpacing : 1,
}

var data1 = data;
var options1 = options;

// Default canvas
$(document).ready(function() {
	// Get the context of the canvas element we want to select
	var ctx = document.getElementById("myChart").getContext("2d");
	var myLineChart = new Chart(ctx).Line(data, options);
});

// Pop up canvas
$('#myModal').click(function() {
	var popupctx = document.getElementById("popupChart").getContext("2d");
	var popupLineChart = new Chart(popupctx).Line(data1, options1);
});


