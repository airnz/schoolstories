/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.gate5a.schoolstories.importer;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;
import nz.gate5a.schoolstories.repo.SchoolNumeracyLiteracyRepository;

import org.junit.Test;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import au.com.bytecode.opencsv.CSVReader;

/**
 *
 * @author rjsang
 */
public class SchoolNumeracyLiteracyCsvImporterTest {
    
    private static final String LITERACY_AND_NUMERACY_STATISTICS_SCHOOL_2014_17042015_CSV = "/Literacy-and-Numeracy-Statistics-School-2014-17042015.csv";

	public SchoolNumeracyLiteracyCsvImporterTest() {
    }

    @Test
    public void testThatAllSchoolsAreRead() throws Exception {
        InputStream in = getClass().getResourceAsStream(LITERACY_AND_NUMERACY_STATISTICS_SCHOOL_2014_17042015_CSV);
        SchoolNumeracyLiteracyRepository repo = mock(SchoolNumeracyLiteracyRepository.class);
        NumeracyLiteracyCreator creator = new NumeracyLiteracyCreator();
        CsvImporter importer = new CsvImporter(in, repo, creator);
        importer.go();
        verify(repo, times(2772)).save(any(SchoolNumeracyLiteracy.class));
    }

    @Test
    public void testCreateSchool() throws UnsupportedEncodingException, IOException {
        InputStream in = getClass().getResourceAsStream(LITERACY_AND_NUMERACY_STATISTICS_SCHOOL_2014_17042015_CSV);
        NumeracyLiteracyCreator importer = new NumeracyLiteracyCreator();
        
        CSVReader reader = new CSVReader(new InputStreamReader(in, "utf-8"));
        // Skip headers and go to first line of schools
        reader.readNext();

        SchoolNumeracyLiteracy school = importer.create(reader.readNext());
        
        assertThat(school.getSchool()).isEqualTo("Taipa Area School");
        
        assertThat(school.getAcademicYear()).isEqualTo(2014);
        assertThat(school.getAchievement()).isEqualTo("Literacy");
    }
    
    @Test
    public void testCreateSchool_LimitTest() throws UnsupportedEncodingException, IOException {
        InputStream in = getClass().getResourceAsStream(LITERACY_AND_NUMERACY_STATISTICS_SCHOOL_2014_17042015_CSV);
        NumeracyLiteracyCreator importer = new NumeracyLiteracyCreator();
        
        CSVReader reader = new CSVReader(new InputStreamReader(in, "utf-8"));
        // Skip headers and go to first line of schools
        reader.readNext();
        
        for(int i = 0; i < 34; i++) {
            reader.readNext();
        }
        SchoolNumeracyLiteracy school = importer.create(reader.readNext());
        
        assertThat(school.getSchool()).isEqualTo("Okaihau College");
        
        assertThat(school.getDecile()).isEqualTo(2);
    }
}
