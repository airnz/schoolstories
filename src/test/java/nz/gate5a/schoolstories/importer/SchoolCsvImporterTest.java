/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.gate5a.schoolstories.importer;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import nz.gate5a.schoolstories.model.School;
import nz.gate5a.schoolstories.repo.SchoolNumeracyLiteracyRepository;
import nz.gate5a.schoolstories.repo.SchoolRepository;

import org.junit.Test;

import au.com.bytecode.opencsv.CSVReader;

/**
 *
 * @author rjsang
 */
public class SchoolCsvImporterTest {
    
    public SchoolCsvImporterTest() {
    }

    @Test
    public void testThatAllSchoolsAreRead() throws Exception {
        InputStream in = getClass().getResourceAsStream("/schooldirectory.csv");
        SchoolRepository repo = mock(SchoolRepository.class);
        SchoolCreator creator = new SchoolCreator();
        CsvImporter importer = new CsvImporter(in, repo, creator);
               
        importer.go();
        verify(repo, times(2546)).save(any(School.class));
    }
//
//    @Test
//    public void testCreateSchool() throws UnsupportedEncodingException, IOException {
//        InputStream in = getClass().getResourceAsStream("/schooldirectory.csv");
//        SchoolCreator importer = new SchoolCreator(in, null);
//        
//        CSVReader reader = new CSVReader(new InputStreamReader(in, "utf-8"));
//        // Skip headers and go to first line of schools
//        for(int i = 0; i < 4; i++) {
//            reader.readNext();
//        }
//        School school = importer.createSchool(reader.readNext());
//        
//        assertThat(school.getName()).isEqualTo("Te Kao School");
//        
//        assertThat(school.getLongitude()).isEqualByComparingTo(new BigDecimal("172.965948"));
//        assertThat(school.getLatitude()).isEqualByComparingTo(new BigDecimal("-34.654712"));
//    }
//    
//    @Test
//    public void testCreateSchool_LimitTest() throws UnsupportedEncodingException, IOException {
//        InputStream in = getClass().getResourceAsStream("/schooldirectory.csv");
//        SchoolCreator importer = new SchoolCreator(in, null);
//        
//        CSVReader reader = new CSVReader(new InputStreamReader(in, "utf-8"));
//        // Skip headers and go to first line of schools
//        for(int i = 0; i < 34; i++) {
//            reader.readNext();
//        }
//        School school = importer.createSchool(reader.readNext());
//        
//        assertThat(school.getName()).isEqualTo("Birkenhead College");
//        
//        assertThat(school.getInternationalStudents()).isEqualTo(35);
//    }
    
    @Test
    public void testColumbaCollege() throws UnsupportedEncodingException, IOException {
        InputStream in = getClass().getResourceAsStream("/schooldirectory.csv");
        CSVReader reader = new CSVReader(new InputStreamReader(in, "utf-8"));
        
        // Skip headers and go to first line of schools
        for(int i = 0; i < 349; i++) {
            reader.readNext();
        }
        String[] fields = reader.readNext();
        assertThat(fields.length).isEqualTo(38);
        
        assertThat(fields[18]).isEqualTo("Primary Co-ed, Secondary Girls");
    }
    
}
