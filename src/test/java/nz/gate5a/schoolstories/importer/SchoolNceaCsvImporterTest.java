/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.gate5a.schoolstories.importer;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import nz.gate5a.schoolstories.model.SchoolNceaQualifications;
import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;
import nz.gate5a.schoolstories.repo.NceaRepository;
import nz.gate5a.schoolstories.repo.SchoolNumeracyLiteracyRepository;

import org.junit.Test;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import au.com.bytecode.opencsv.CSVReader;

/**
 *
 * @author rjsang
 */
public class SchoolNceaCsvImporterTest {
    
    private static final String NCEA_STATISTICS_SCHOOL_2014_17042015_CSV = "/Qualification-Statistics-School-2004-21032013.csv";

	public SchoolNceaCsvImporterTest() {
    }

    @Test
    public void testThatAllResultsAreRead() throws Exception {
        InputStream in = getClass().getResourceAsStream(NCEA_STATISTICS_SCHOOL_2014_17042015_CSV);
        NceaRepository repo = mock(NceaRepository.class);
        NceaQualificationCreator creator = new NceaQualificationCreator();
        CsvImporter importer = new CsvImporter(in, repo, creator);
        importer.go();
        verify(repo, times(5068)).save(any(SchoolNceaQualifications.class));
    }

 
}
