/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.gate5a.schoolstories;

import java.util.List;

import javax.inject.Inject;

import nz.gate5a.schoolstories.model.SchoolNumeracyLiteracy;
import nz.gate5a.schoolstories.repo.SchoolNumeracyLiteracyRepository;

import org.fest.assertions.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author rjsang
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SchoolStories.class)
@WebAppConfiguration
public class SchoolStoriesTest {
    
    @Inject
    private SchoolNumeracyLiteracyRepository repo;
    
    @Test
    @Ignore
    public void testGlendowie() {
        List<SchoolNumeracyLiteracy> l = repo.findBySchool(new PageRequest(0, 1000), "\"Glendowie School\"");
        Assertions.assertThat(l).isEmpty();
        
        List<SchoolNumeracyLiteracy> l2 = repo.findBySchool(new PageRequest(0, 1000), "\"Glendowie College\"");
        Assertions.assertThat(l2).isNotEmpty();
    }
    
}
